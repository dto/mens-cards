;;; mens-meeting-cards.el --- customizable SVG game cards for men's groups

;; Copyright (C) 2018, 2019 David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: games
;; License: MIT (see below)

;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;;; Commentary:

;; See the included file "README.md" for full commentary.

;; The main command is "M-x mmc-make-all-cards". You should customize
;; the variables first. See the bottom of the file for an example
;; customization.

;;; Code:

(require 'cl)
(require 'svg)

(defmacro defvar* (symbol initvalue &optional docstring)
  `(progn (defvar ,symbol nil docstring)
	  (setf ,symbol ,initvalue)))

;;; Customization variables

(defvar* mmc-version-string "v2.6i")
(defvar* mmc-card-width 2.2) ;; in inches
(defvar* mmc-card-height 3.43) ;; in inches
(defvar* mmc-card-header-height 0.8) ;; in inches
(defvar* mmc-x-offset 0.32) ;; in inches
(defvar* mmc-y-offset 0.558) ;; in inches

(defvar* mmc-background-color "white")

(defvar* mmc-text-font-color "#000000")
(defvar* mmc-text-font-family "sans-serif")
(defvar* mmc-text-font-size "11.2")
(defvar* mmc-text-font-weight "normal")

(defvar* mmc-title-font-color "#ffffff")
(defvar* mmc-title-font-family "sans-serif")
(defvar* mmc-title-font-size "16")
(defvar* mmc-title-font-weight "normal")

(defvar* mmc-inkscape-program "inkscape");; change this if you need to customize the path

(defvar* mmc-category-colors '("#ee0000" "#0000ee" "#00cd00" "#eeee00" "#888888"))
(defvar* mmc-categories '(:activity :topic :merit :special :back))

(defvar* mmc-include-gay-bi-topics t)

;;; Topic cards

(defvar* mmc-general-topics '(
"getting to know you"
"listening with compassion"
"teamwork and camaraderie"
"life goals"
"the very happiest I ever was"
"the worst day of my life"
"dealing with difficult people"
"letting go 
/ holding on"
"men’s life stages"
"forgiving others 
/ being forgiven"
"dealing with failure 
/ dealing with success"
"bad habits"
"healthy friendships 
/ unhealthy friendships"
"difficulty making friends"
"self-concept"
"self-esteem"
"self-actualization"
"self-image 
/ distorted self image"
"negative body image 
/ positive body image"
"mid-life passage"
"my childhood"
"my teens"
"my twenties"
"my thirties"
"my forties"
"my fifties"
"my sixties"
"my seventies"
"casual sex"
"aging"
"violence"
"depression"
"coping with loss 
and grief"
"coping with change 
and uncertainty"
"God"
"faith"
"love"
"myths about men"
"myths about women"
"work"
"addiction"
"procrastination"
"stoicism"
"erectile dysfunction"
"my mother"
"my father"
"the male body"
"the men I admire"
"the women I admire"
"marriage"
"being a father"
"celibacy"
"women"
"divorce"
"HIV"
"open relationships"			      
"loss of a loved one"
"intimate relationships 
/ lack of intimacy"
"emotional distance"
"sexual difficulties"
"sexual experience 
/ lack of sexual experience"
"personal distance"
"social anxiety"
"body language"
"breakups" 
"a night to remember"
"a night to forget"
"being a wallflower"
"memories of puberty"
"feeling invisible"
"late blooming"
"my life was affected 
by a suicide"
"the prostate"
"changing careers"
"how could you do this 
to me?"
"grief 
/ trauma"
"post-traumatic stress
/ post-traumatic growth"
"i don’t want to talk about it 
/ you just don’t understand"
"fitness and sports"
"injuries"
"I should’ve done this 
years ago 
/ I never should have 
done that"
"beards"
"monogamy"
"circumcision"
"first times"
"spirituality"
"why can’t I be more like you 
/ I worry I’ll end up like you"
"saying the wrong thing"
"women's life stages"
"being an uncle"
"public displays of affection"
"he changed my life
for the better"
"she changed my life
for the better"
"testicular self-exam"		      
"the female body"		      
"being a son"
"condoms"			      
"being a friend"
"let’s just be friends 
/ “friends with benefits”"
"i love you as a friend 
/ i don’t love you anymore"
"friendships with men"
"understanding women 
/ not understanding women"
"conflicts with other men 
/ harmony with other men"
"understanding other men 
/ not understanding other men"
"feeling ignored 
/ feeling scrutinized"
"second chances"
"May-December romance"
"missed opportunities"
"awesome things about me"))

(defvar* mmc-gay-bi-topics '(
"butch 
/ femme"
"gay/bi history"
"homophobia"
"gaydar"
"anal virginity"
"top 
/ bottom"
"passing as straight"
"when did you know...?"
"coming out"
"“not into the scene” 
/ “new to this”"
"PREP (pre-exposure
propylaxis)"			      
"internalized homophobia"
"bear 
/ otter 
/ twink"
"city guys 
/ country guys"
"“masc only”"
))

;;; Activity cards

(defvar* mmc-activity-cards '(
"\"The Bragging Game\"

For several minutes, talk
about how proud you are of
some personal quality,
accomplishment, or
relationship in your life. 
Don't be shy! This card 
gives you permission to be
up front with positive 
feelings."

"\"Question Time\"

The card holder announces a
question, and everyone takes
a minute or two to write
down some thoughts. Then go
around in a circle and briefly
share responses."

"\"The Empty Chair\"

Sit facing an empty chair 
and put whatever is \"keeping 
you down\" into the chair. A 
person, a relationship, a habit,
or part of your own self
that is causing conflict or
resistance.

Talk to the chair and explain 
your feelings."

"\"Here and now\"

Each man in turn takes one
minute being aware of the 
current environment and making 
statements like \"Now I am 
aware of...\" or \"I notice 
right now I am...\" without
worrying about responses."

"\"The Flattery Game\"

Everyone gets a turn to say
something positive and 
encouraging to the card 
holder.

Alternatively, the card holder
can choose one or more
people to say positive things
to."

"\"Mystery Man\"

Each person writes down a
question about the card 
holder, and then the card 
holder reads the questions
aloud and answers them if 
desired."

"\"Selected Reading\"

The group leader will choose 
a relevant reading selection
of about five minutes in 
length. After reading, the 
group can respond and discuss."

"\"The Observer\"

Take a moment to notice the 
situation around you. How 
are people sitting? Who has a
relaxed posture, and who is 
more tense? Does anyone have
an untied shoelace? Who is 
more shy and who is more 
outspoken? Write down an 
observation about each
man in the group, and read
them aloud."
))

(defvar* mmc-special-cards '(
:transfer

"Choose one of your other 
cards and transfer it to 
another group member for
his response.

The other group member may
decline the card."

:emotion-map

"Circle three emotions on 
the emotion map provided
by your group leader.

Discuss all three emotions
you chose. Try to see clearly
your emotional state and
communicate it.

Others can respond as
appropriate."

:roundabout

"Choose one of your topic
cards. Instead of giving a solo
response to that card, go
around the circle with each
man giving a brief response."
))

(defvar* mmc-merit-cards '(
"One point of positive merit.

Use this to show appreciation
for another member's story,
response, or activity."))

(defvar* mmc-back-cards
    (list (concat "Men's Meeting Cards " mmc-version-string)))
			
;;; SVG Card generation

(defun mmc-category-color (category)
  (let ((n (position category mmc-categories)))
    (if (numberp n)
	;; use last color if too many categories
	(nth (min n (1- (length mmc-category-colors)))
	     mmc-category-colors)
      (car (last mmc-category-colors)))))

(defun mmc-inches (number)
  (format "%Sin" number))

(defun mmc-pixels (number)
  (format "%Sin" number))

(defun mmc-blank-card ()
  (svg-create (mmc-inches mmc-card-width)
	      (mmc-inches mmc-card-height)))

(defvar* mmc-card-number 0)
(defvar* mmc-current-card nil)
(defvar* mmc-x 0.0)
(defvar* mmc-y 0.0)
(defvar* mmc-tx 0.0)
(defvar* mmc-ty 0.0)

(defun mmc-reset-card ()
  (setf mmc-current-card (mmc-blank-card))
  (setf mmc-x mmc-x-offset)
  (setf mmc-y mmc-y-offset)
  (setf mmc-tx 0.0)
  (setf mmc-ty 0.0))

(defun* mmc-draw-card (category title text &optional (x 0) (y 0))
  ;; draw background
  (svg-rectangle mmc-current-card
		 (mmc-inches x)
		 (mmc-inches y)
		 (mmc-inches mmc-card-width)
		 (mmc-inches mmc-card-height)
		 :fill mmc-background-color)
  ;; draw header color
  (svg-rectangle mmc-current-card
		 (mmc-inches x)
		 (mmc-inches y)
		 (mmc-inches mmc-card-width)
		 (mmc-inches mmc-card-header-height)
		 :fill (mmc-category-color category))
  ;; draw title text
  (svg-text mmc-current-card
	    title
	    :font-size mmc-title-font-size
	    :font-weight mmc-title-font-weight
	    :fill mmc-title-font-color
	    :font-family mmc-title-font-family
	    :x (mmc-inches (+ x mmc-x-offset))
	    :y (mmc-inches (+ y mmc-y-offset)))
  ;; draw lines
  (mmc-draw-paragraph (split-string text "\n") (+ x mmc-x-offset) y)
  ;; draw card number
  (unless (zerop mmc-card-number)
    (svg-text mmc-current-card (format "%S" mmc-card-number)
	      :font-size "9"
	      :font-weight mmc-text-font-weight
	      :fill mmc-text-font-color
	      :font-weight mmc-text-font-weight
	      :font-family mmc-text-font-family
	      :x (mmc-inches 1.7)
	      :y (mmc-inches 3.1))))

(defun mmc-draw-paragraph (lines x y)
  (when lines
    (let ((baseline (+ y mmc-card-header-height 0.4)))
      (dolist (line lines)
	(svg-text mmc-current-card
		  line
		  :font-size mmc-text-font-size
		  :fill mmc-text-font-color
		  :font-weight mmc-text-font-weight
		  :font-family mmc-text-font-family
		  :x (mmc-inches (+ x 0.0))
		  :y (mmc-inches baseline))
	(incf baseline 0.15)))))

;;; Generating all the cards

(defun mmc-write-card ()
  (with-temp-buffer
    (svg-print mmc-current-card)
    (write-file (format "card-%S.svg" mmc-card-number))))

(defun mmc-convert-card ()
  (ignore-errors
    (call-process mmc-inkscape-program
		  nil '(:file "mmc-errors.txt") nil
		  (format "--export-png=card-%S.png" mmc-card-number)
		  "--export-dpi=600"
		  (format "card-%S.svg" mmc-card-number))))

(defun mmc-make-card (category title text)
  (mmc-reset-card)
  (message "Generating card %S..." mmc-card-number)
  (mmc-draw-card category title text)
  (message "Generating card %S... Done." mmc-card-number)
  (mmc-write-card)
  (mmc-convert-card)
  (message "Finished card %S." mmc-card-number)
  (incf mmc-card-number))

(defun mmc-make-all-cards ()
  (interactive)
  (setf mmc-card-number 0)
  (setf mmc-background-color "#888888")
  (setf mmc-text-font-color "#ffffff")
  (mmc-make-card :back " " (first mmc-back-cards))
  (setf mmc-background-color "white")
  (setf mmc-text-font-color "#000000")
  (dolist (topic (if mmc-include-gay-bi-topics
		     (append mmc-general-topics mmc-gay-bi-topics)
		   mmc-general-topics))
    (mmc-make-card :topic "topic" topic))
  (dotimes (n 3)
    (dolist (activity mmc-activity-cards)
      (mmc-make-card :activity "activity" activity)))
  (dotimes (n 16)
    (mmc-make-card :merit "merit" (first mmc-merit-cards)))
  (setf mmc-title-font-color "black")
  (dotimes (n 3)
    (mmc-make-card :special "transfer" (getf mmc-special-cards :transfer)))
  (dotimes (n 3)
    (mmc-make-card :special "roundabout" (getf mmc-special-cards :roundabout)))
  (dotimes (n 3)
    (mmc-make-card :special "emotion map" (getf mmc-special-cards :emotion-map))))

;;; Example customization

(defun mmc-test ()
  (interactive)
  (setf mmc-include-gay-bi-topics t)
  (setf mmc-title-font-family "Roboto")
  (setf mmc-text-font-family "Roboto")
  (mmc-make-all-cards))

;;; Postamble

(provide 'mens-meeting-cards)

;; Local Variables:
;; lexical-binding: t
;; End:

;;; mens-meeting-cards.el ends here
