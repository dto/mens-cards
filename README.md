# Men's Meeting Cards v2.5

This repository contains a customizable set of 139 free playing card images
designed for use with men's discussion groups. These cards create a
mix of simple mini-games and suggested topics to spark
interesting conversations. The topics include various aspects of men's
lives, including men's health concerns and optional cards of special
concern to gay and bisexual men. Unneeded cards can simply be omitted.

To download the repository and all its images, use the download button
above (it has a little cloud on it with an arrow pointing down) or
click the following link:
https://gitlab.com/dto/mens-cards/-/archive/master/mens-cards-master.zip

See the included image "preview.jpg" for a photo of the previous
version's finished cards. You can find the current card images in the
subfolder "v2.5-cards". The image "card-0.jpg" is for the back side of
all the cards.

I would like to expand the topic list, please send suggestions and
other ideas to the email address below.

# Making real playing cards

Whether or not you customize the cards, you can have them printed by
Makeplayingcards.com by uploading the PNG versions to their online
card creator. Choose "US Game Deck Size Cards", 2.2x3.43in
(56×87mm). This aspect will eventually be customizable so that
the program is not US-centric.

These files have not been tested with other card manufacturers. I
ordered a printed copy of the version 1.2 cards, and found them to be
high quality with sharp text and bright color. 

# Ready-to-order cards

If you don't want to deal with the somewhat tricky card creator
software, I have submitted a copy of this design to the
MakePlayingCards online store for you to purchase your own cards
easily. Here is the link: https://www.makeplayingcards.com/sell/marketplace/mens-meeting-cards-v25.html

Disclosure: the minimum markup allowed on the MPC online store is 10%,
so if you buy the ready-to-order version, I will be receiving about
$2.50 for each deck sold. Proceeds (if any) will be used exclusively
to support our group, where we are refining and expanding the cards
with new ideas and topics.

Please let me know if you have any questions, comments, or
suggestions. I hope these cards can help start conversations in your
own groups.

# Rules for the card game

1. Deal. The group leader deals 6 to 8 cards to each player.

2. Choose a card. Starting with the player to the left of the group
leader, each player chooses a card from his hand. (If you don't like
one of your cards, put it face down on the table instead.)

3. Respond. If it’s a topic, give a response. If it’s an activity, follow
the instructions.  The group leader can provide any worksheets
(emotion map) or idea sheets related to the card.

4. Listen. After the response, other players can raise their hands to
give constructive responses or share something relevant.

5. (optional) Reward. The group can decide to give a merit card for a well spoken
response or constructive activity. The group leader will keep an eye
on the clock to make sure everyone gets enough turns.

# Acknowledgements

Special thanks to the Men's Group Manual for some of the activity
ideas, and to the men of Worcester Areios Men's Group for invaluable
help in brainstorming and testing the card game.

# Extra resources

A simple public-domain emotion wheel from Wikipedia is included in the
printable file "emotion-wheel.svg". There are fancier wheels with far
more emotional gradations listed, but we cannot include them for
copyright reasons.

You can easily find one on the web by searching for "emotion map" or
"emotion wheel". Try adding "pdf" to your search query if you want to
print them out.

The "idea sheets" mentioned in the cards are not yet included in the
repository, but will be, and I would like to include them in a
printable instruction manual.

# Permissive license

The cards and the generator program are released under the MIT license
so that people can freely modify the cards for their own purposes,
including translation into other languages. See the included text file
"LICENSE" for complete information.

# Customizing the cards

A customizable program to generate the cards in SVG format is
included. You can add new cards and remove ones you don't need, making
your own custom deck; you can also change the design, fonts,
formatting, language, and so on if desired.

With Inkscape installed, you can also automatically convert the
cards to PNG format for easy printing.

The card generator is written in Emacs Lisp and thus requires GNU
Emacs to work. Probably you should ask an Emacs user to help you
customize the cards. If you can't find one, email me at the address
below.

# Contact

David O'Toole (deeteeoh1138@gmail.com)
